# `exifally`

### Wrapper functions and helper tools for the `exiftool` project

#### Helper scripts: 
 - wipe\_exif\_data.sh
 - file\_randomizer.sh

---

`wipe_exif_data.sh`

**Destruction level:** Complete Amnesia

This wrapper function finds `exiftool` supported image formats and removes all uniquely identifiable exifdata and metadata info.

Original image files are deleted, only modiified files are kept.

eg:

```
 ./wipe_exif_data.sh
 Found [       7] .jpg files for EXIF removal
    7 image files updated
 Deleting original copies  marked [*.jpg_original]

 Found [       1] .png files for EXIF removal
    1 image files updated
 Deleting original copies  marked [*.png_original]
```

---

`file_randomizer.sh`

**Destruction level:** Unrecognizable filenames

Small helper script to rename a path of image files


- Input : Path with any supported image file type
- Output: _DSC\_\[CCYYMMDD\]\_\[1-9999\]\_[Aa-Zz\\0-9].jpg_

eg:

```
./file_randomizer.sh
Renaming file "Screen Shot 2017-06-14 at 2.37.18 PM.png" to "DSC_20190221_0001_czYuj.jpg"
Renaming file "Screen Shot 2018-01-18 at 7.22.01 AM.png" to "DSC_20190221_0002_MgLNN.jpg"
```

---

### Command examples: 

##### Terminal one-liner for complete rename and wipe of images in path

**Destruction level:** Nuclear

```
IFS= here=($PWD);find . -type d|while IFS= read -r foo;do cd "$foo"; file\_randomizer.sh; wipe_exif_data.sh;cd  "$here";done
```

Legibility:

```
IFS=
here=($PWD)
find . -type d|\
  while IFS= \
  read -r foo
  do 
    cd "$foo"
    file_randomizer.sh
    wipe_exif_data.sh
    cd  "$here"
  done
```
