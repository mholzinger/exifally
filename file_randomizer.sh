#!/bin/bash

# Override any UTF-8 encoding for `tr` command - set locally
LC_ALL=C
LANG=""

image_files=(*.[JjPp][PpNn][Gg])
cnt=${#image_files[@]}

for (( idx = 0; idx < cnt; idx++ ))
do
  salty=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 5 | head -n 1)

  DAY=$( date +%Y%m%d )
  file_number=$( echo $idx + 1 | bc )
  JNUM=$(printf "%04d\n" $file_number)
  file_name=DSC_"$DAY""_$JNUM"_"$salty".jpg

  # Echo something nice for the user to see
  echo Renaming file \""${image_files[idx]}"\" to \""$file_name"\"

  # Rename the file
  mv "${image_files[idx]}" "$file_name"
done
